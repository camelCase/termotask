import numpy as numpy
import math
import time
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
plotly.tools.set_credentials_file(username='nikitmit', api_key='7hPkf2OsQOCCRCMODxZj')


def lmbda(t): return 0.0134 * (1 + 0.000435 * t)


def f(t, z): return F0 * math.exp(- k(t) * z)


def k(value): return numpy.interp(value, [293, 1278, 1528, 1677], [0.002, 0.005, 0.0078, 0.01])


Uoc = 300  # Kelvins - initial temperature for every point of the plate
epsilon = 1e-2
Ft = 10
F0 = 10
alpha = 0.01
Nx = 4
Nz = 4
hx = 2.7
hz = 1.8

big_matrix_size = Nx * Nz
lambdas = numpy.full((Nz, Nx), lmbda(Uoc))
temperatures = numpy.full((big_matrix_size), Uoc)
A = numpy.zeros((big_matrix_size, big_matrix_size))  # Initially with zeros
b = numpy.zeros((big_matrix_size))  # Initially with zeros


def k_i_j_minus_1(i, j): return (lambdas[i][j - 1] + lambdas[i][j]) / 2 / hx ** 2


def k_i_j_plus_1(i, j): return (lambdas[i][j + 1] + lambdas[i][j]) / 2 / hx ** 2


def k_i_j(i, j):
    return - ((lambdas[i][j + 1] + lambdas[i][j - 1]) / 2 + lambdas[i][j]) / hx ** 2 \
           - ((lambdas[i + 1][j] + lambdas[i - 1][j]) / 2 + lambdas[i][j]) / hz ** 2


def k_i_minus_1_j(i, j): return (lambdas[i - 1][j] + lambdas[i][j]) / 2 / hz ** 2


def k_i_plus_1_j(i, j): return (lambdas[i + 1][j] + lambdas[i][j]) / 2 / hz ** 2


def fill_matrix_of_system():
    for i in range(Nx, Nx * (Nz - 1)):  # 1
        if i % Nx == 0 or (i + 1) % Nx == 0:
            continue  # borders processed in 2-5 equations
        big_row = i
        row = i // Nx
        col = i % Nx
        i_j_minus_1 = i - 1
        i_j_plus_1 = i + 1
        i_j = i
        i_minus_1_j = i - Nx
        i_plus_1_j = i + Nx

        A[big_row][i_j_minus_1] = k_i_j_minus_1(row, col)
        A[big_row][i_j_plus_1] = k_i_j_plus_1(row, col)
        A[big_row][i_j] = k_i_j(row, col)
        A[big_row][i_plus_1_j] = k_i_plus_1_j(row, col)
        A[big_row][i_minus_1_j] = k_i_minus_1_j(row, col)
        b[big_row] = f(temperatures[i_j], hz * row)  # __temperatures[row, col], was col ???

    for j in range(Nx):  # 2
        big_row = j  # just for readability - row of big matrix
        _0_j = j
        _1_j = j + Nx
        A[big_row][_0_j] = 1
        A[big_row][_1_j] = -1
        b[big_row] = hz / lambdas[0][j] * Ft  # lambdas was [j][j]

    for i in range(1, Nx - 1):  # 3
        big_row = i * Nx  # just for readability - row of big matrix
        i_0 = big_row  # just for readability
        i_1 = i_0 + 1  # just for readability
        A[big_row][i_0] = 1 - (alpha * hx) / lambdas[i][0]
        A[big_row][i_1] = -1
        b[big_row] = -((alpha * hx) / lambdas[i][0]) * Uoc

    for i in range(1, Nz - 1):  # 4  # was Nx
        big_row = (i + 1) * Nx - 1  # just for readability - row of big matrix
        i_nx_1 = big_row  # just for readability
        i_nx_2 = big_row - 1  # just for readability
        A[big_row][i_nx_1] = -(1 + alpha * hx / lambdas[i][Nx - 1]) # was lambda[self.Nx - 1][i]
        A[big_row][i_nx_2] = 1
        b[big_row] = -(alpha * hx / lambdas[i][Nx - 1]) * Uoc

    for j in range(0, Nx):  # 5
        big_row = Nx * (Nz - 1) + j  # just for readability - row of big matrix
        nz_1_j = big_row  # just for readability
        nz_2_j = big_row - Nx  # just for readability
        A[big_row][nz_2_j] = 1
        A[big_row][nz_1_j] = - (1 + alpha * hz / lambdas[Nz - 1][j]) # was lambda[i][self.Nx - 1]
        b[big_row] = -alpha * hz / lambdas[Nz - 1][j] * Uoc


def fill_lambdas(temperatures):
    for i in range(Nz):
        for j in range(Nx):
            lambdas[i][j] = lmbda(temperatures[i][j])


def converges(temperatures, temperatures_tmp):
    return numpy.max(numpy.abs(temperatures - temperatures_tmp)) < epsilon


def solve():
    global temperatures, A
    numpy.set_printoptions(precision=2)
    start_time = time.time()
    iterations_count = 0
    temperatures_tmp = numpy.full((big_matrix_size), Uoc)
    while True:
        iterations_count += 1
        temperatures = temperatures_tmp
        A = numpy.zeros((big_matrix_size, big_matrix_size))
        fill_matrix_of_system()
        print(A)
        temperatures_tmp = numpy.linalg.solve(A, b)
        fill_lambdas(temperatures_tmp.reshape((Nz, Nx)))
        if converges(temperatures, temperatures_tmp) or iterations_count > 10:
            temperatures = temperatures_tmp
            break

    print('{0} iterations took {1} seconds!'.format(iterations_count, time.time() - start_time))
    print(temperatures.reshape((Nz, Nx)))

    return temperatures


def show_plot_1():
    data = [go.Surface(z=temperatures)]
    layout = go.Layout(
        title='Termo plot', autosize=False, width=500, height=500,
        margin=dict(l=65, r=50, b=65, t=90)
    )
    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='elevations-3d-surface')

if __name__ == "__main__":
    temperatures = solve().reshape(Nz, Nx)
    show_plot_1()
    # numpy.savetxt('temperatures' + time.strftime("%Y%m%d%H%M%S") + '.csv', temperatures, delimiter=',')
